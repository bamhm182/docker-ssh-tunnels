#!/usr/bin/python3

import argparse
import os
import subprocess
import time
import yaml
from classes import *


class Tunnels:
    def __init__(self):
        self.time = time.strftime("%Y%m%d-%H%M%S", time.localtime())
        self.service_set = None
        self.network_set = None
        self.dir = os.path.dirname(os.path.realpath(__file__))

        self.compose_dir = f"{self.dir}/compose"
        self.time_dir = f"{self.compose_dir}/{self.time}"
        if not os.path.exists(f"{self.compose_dir}/current"):
            os.symlink(f"{self.compose_dir}/default", f'{self.compose_dir}/current')
        self.working_dir = f"{self.compose_dir}/current"

        subprocess.call('clear' if os.name == 'posix' else 'cls')

        self.process_flags()

    def process_flags(self):
        parser = argparse.ArgumentParser()
        group = parser.add_mutually_exclusive_group()
        group.add_argument('-g', '--generate', help='Create a and start new environment', action='store_true')
        group.add_argument('-k', '--kill', help='Stop and tear down current environment', action='store_true')
        group.add_argument('-s', '--start', help='Start current environment', action='store_true')
        args = parser.parse_args()
        self.check_dependencies()
        if args.generate:
            self.stop()
            self.write_files(self.generate())
            self.start()
        elif args.start:
            self.stop()
            self.start()
        elif args.kill:
            self.stop()
        else:
            parser.print_help()

    def generate(self):
        print("Generating docker-compose.yml...")
        self.network_set = self.generate_networks()
        self.service_set = self.generate_services(self.network_set.networks)

        return {
            'version': '3.7',
            'services': self.service_set.export(),
            'networks': self.network_set.export()
        }

    @staticmethod
    def generate_networks():
        networks = NetworkSet([HostNetwork()])
        for i in range(4):
            networks.add_network()
        return networks

    @staticmethod
    def generate_services(networks):
        services = ServiceSet()

        # Add start
        services.add_start(networks[0:2])

        # Add pivots
        for i in range(1, len(networks) - 1):
            services.add_pivot(networks[i:i + 2], f'pivot{i}')

        # Add a target
        services.add_target([networks[-1]])
        return services

    def write_files(self, data):
        if os.path.exists(f"{self.compose_dir}/current"):
            os.remove(f"{self.compose_dir}/current")
        os.symlink(self.time_dir, f'{self.compose_dir}/current')

        os.mkdir(self.time_dir)
        with open(f'{self.time_dir}/docker-compose.yml', 'w') as file:
            output = yaml.dump(data, Dumper=yaml.Dumper, indent=2, default_flow_style=False)
            output = output.replace("subnet:", "- subnet:").replace("__TIME_DIR__", self.time_dir)
            file.write(output)

        with open(f'{self.time_dir}/motd', 'w') as file:
            file.write('\n'.join(self.service_set.get_motd()))
            file.write('\n\n')

    def start(self):
        file = f"{self.working_dir}/docker-compose.yml"
        commands = [
            ("Checking docker-compose.yml...", ["docker-compose", "-f", file, "config"]),
            ("Building docker-compsose.yml...", ["docker-compose", "-f", file, "build"]),
            ("Starting docker-compose.yml...", ["docker-compose", "-f", file, "up", "-d"])
        ]
        for p, c in commands:
            print(p)
            if subprocess.call(c, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL) != 0:
                print("\tFailed...")
                exit(1)
        try:
            for net, curr_ip in self.service_set.services[0].ips:
                if net == self.network_set.networks[0].name:
                    ip = curr_ip
                    break
        except AttributeError:
            out, err = subprocess.Popen(['docker', 'container', 'list'], stdout=subprocess.PIPE).communicate()
            for c in out.splitlines():
                c = str(c)
                if "ssh_tunnels:start" in c:
                    cid = c.split(' ')[0].replace("b'", "")
                    out, err = subprocess.Popen(['docker', 'inspect', cid], stdout=subprocess.PIPE).communicate()
                    for line in out.splitlines():
                        line = str(line)
                        if '"IPAddress"' in line and '.2",' in line:
                            ip = line.split('": "')[1].replace('",\'', '')
                            break

        subprocess.call('clear' if os.name == 'posix' else 'cls')

        print("Entering container with:")
        print(f"\t ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@{ip}")
        print("\t PASSWORD: root\n\n\n")
        subprocess.call(["ssh", "-o", "UserKnownHostsFile=/dev/null", "-o", "StrictHostKeyChecking=no", f"root@{ip}"])

    def stop(self):
        print("Stopping previous docker-compose.yml...")
        cmd = ["docker-compose", "-f", f"{self.working_dir}/docker-compose.yml", "down", "--remove-orphans"]
        if subprocess.call(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL) != 0:
            print("\tFailed...")

    @staticmethod
    def check_dependencies():
        print("Checking Dependencies...")
        for dep in ['docker', 'docker-compose']:
            try:
                subprocess.check_call([dep, '-v'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            except OSError:
                print(f"\t{dep} is not installed. Please install {dep}.")
                exit()

    @staticmethod
    def dict_to_list(dic):
        return [{key: value} for key, value in dic.items()]


if __name__ == "__main__":
    Tunnels()
