import random
import string
import ipaddress


class NetworkSet:
    def __init__(self, networks=None):
        self.networks = networks if networks else []

    def add_network(self, network=None):
        self.networks.append(network if network else InternalNetwork())

    def export(self):
        return {network.name: network.export() for network in self.networks}


class BasicNetwork:
    def __init__(self):
        self.name = f"net-{''.join(random.choice(string.ascii_letters) for i in range(5))}"
        self.subnet = self.choose_subnet(24)
        self.driver = 'bridge'
        self.internal = 'false'
        self.used_ips = []

    def export(self):
        return {
                'driver': self.driver,
                'internal': self.internal,
                'ipam': {
                    'config': {
                        "subnet": str(self.subnet)
                    }
                }
            }

    @staticmethod
    def choose_subnet(cidr):
        if 8 <= cidr <= 30:
            return random.choice(list(ipaddress.IPv4Network('10.0.0.0/8').subnets(new_prefix=cidr)))
        else:
            print("Invalid CIDR requested for network. Choose value between /8 and /30 (inclusive)")
            exit()


class HostNetwork(BasicNetwork):
    def __init__(self):
        super(HostNetwork, self).__init__()
        self.subnet = self.choose_subnet(30)
        self.used_ips = [str(self.subnet.network_address+1)]


class InternalNetwork(BasicNetwork):
    def __init__(self):
        super(InternalNetwork, self).__init__()
        self.driver = 'macvlan'
        self.internal = 'true'

