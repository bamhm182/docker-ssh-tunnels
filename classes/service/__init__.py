import random
import string

from classes import InternalNetwork
from classes.service.hints import HINTS


class ServiceSet:
    def __init__(self, services=None):
        self.services = services if services else []

    def add_start(self, networks, name="start"):
        self.services.append(StartService(networks, name))

    def add_pivot(self, networks, name="pivot"):
        self.services.append(PivotService(networks, name))

    def add_target(self, networks, name="target"):
        self.services.append(TargetService(networks, name))

    def get_motd(self):
        lst = []
        for service in self.services:
            try:
                for hint in HINTS[service.image]:
                    if hint not in lst:
                        lst.append(hint)
            except KeyError:
                pass

        lst = ["\nWelcome!\n\nHere are some hints to get you started:\n"] + lst if lst else ["\nWelcome!\n"]
        return lst

    def export(self):
        return {service.name: service.export() for service in self.services}


class BasicService(object):
    def __init__(self, networks, name='basic'):
        self.service_list = []
        self.image = ''
        self.name = name
        self.hostname = ''
        self.ips = []
        self.networks = networks

        self.select_ip(networks)

        self.set_name(name)

    def set_name(self, name):
        self.hostname = name
        self.name = f"{name}-{''.join(random.choice(string.ascii_letters) for i in range(5))}"

    def set_image(self, image=''):
        self.image = image if image != '' else random.choice(self.service_list)

    def add_network(self, network=None):
        self.networks.append(network if network else InternalNetwork())

    def select_ip(self, networks):
        for network in networks:
            if len(network.used_ips) == len(list(network.subnet.hosts())):
                continue
            while True:
                ip = str(random.choice(list(network.subnet.hosts())))
                if ip not in network.used_ips:
                    break
            network.used_ips.append(ip)
            self.ips.append((network.name, ip))

    def export(self):
        return {
            'image': self.image,
            'hostname': self.hostname,
            'networks': {net: {"ipv4_address": ip} for net, ip in self.ips}
        }


class StartService(BasicService):
    def __init__(self, networks, name='start'):
        super(StartService, self).__init__(networks)
        self.service_list += ["bamhm182/ssh_tunnels:start"]
        self.volumes = ['./motd:/etc/motd:ro', '../../images/check.py:/usr/bin/check:ro']
        self.set_image()
        self.set_name(name)

    def export(self):
        dic = super(StartService, self).export()
        dic["volumes"] = self.volumes
        return dic


class PivotService(BasicService):
    def __init__(self, networks, name='pivot'):
        super(PivotService, self).__init__(networks)
        self.service_list += ["bamhm182/ssh_tunnels:pivot"]
        self.set_image()
        self.set_name(name)


class TargetService(BasicService):
    def __init__(self, networks, name='target'):
        super(TargetService, self).__init__(networks)
        self.service_list += ["bamhm182/ssh_tunnels:target"]
        self.set_image()
        self.set_name(name)
