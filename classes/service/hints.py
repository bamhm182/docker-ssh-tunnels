HINTS = {
    "bamhm182/ssh_tunnels:start": [
        "- Once properly configured, check your configuration by running the command 'check' on the start machine."
    ],
    "bamhm182/ssh_tunnels:target": [
        "- The target has a web server on port 80.",
        "- Make it so you can view this web server through a local port on this machine."
    ]
}