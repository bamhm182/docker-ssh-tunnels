#!/usr/bin/python3

import requests


class Checker:
    def __init__(self):
        self.answer = "Congratulations!!"
        self.port = input("Enter your port: ")
        self.check_html()

    def check_html(self):
        try:
            content = requests.get(f'http://127.0.0.1:{self.port}').content
            if self.answer in str(content):
                print("Congratulations!! You have successfully completed this challenge!")
                exit(0)
        except requests.exceptions.ConnectionError:
            print(f"Port {self.port} does not appear to be a valid http server")
            exit(1)
        print("Please try again...")


if __name__ == '__main__':
    Checker()
