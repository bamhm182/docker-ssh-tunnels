This project is intended to be used to learn ssh tunneling.

# Dependencies
* python3
* docker
* docker-compose

# Usage


There is a python3 script titled tunnels.py. This is run with several flags:
* -g (--generate): Generate a new, random, docker-compose environment
* -s (--start): Start the most recently created docker-compose environment
* -k (--kill): Kill/Tear down the most recently created docker-compose environment

Once you have docker and docker-compose installed, run the following commands:
```
./tunnels.py -g
./tunnels.py -s
```
Once you are finished with the environment, and before creating a new
environment, run:
```
./tunnels.py -k
```

Once the network has been started, you will be given an IP address of the
starting docker container. You can either start attacking from your local box,
using this as the first pivot, or you can start attacking from the starting
docker container.

Run:
ssh root@[IP]

I use the 'master' branch as a stable "safe zone." If you want to try and use
what I'm currently working on, or if you're having issues with master that I
may have addressed, you can try the "beta" version on the dev branch with the
following command:

```
git checkout dev
```

**Note: I have only tested this on Ubuntu 18.04. I think it should work
anywhere, but it has been untested. Windows may have issues with folder
locations. I may get around to improving how I handle folders. If you don't
want to run it on your local box, Digital Ocean is super cheap and it works
fine there.**


## Digital Ocean

If you do not want to run this on your local machine, you can follow these
generic instructions:
* Create a new Digital Ocean Droplet with the following options:
	* Marketplace: Docker X:xx.xx.xx~x on 18.04
	* Standard: $5/mo, $0.007/hr
	* Leave everything else as default
* SSH into your droplet and run the following commands
```
git clone https://gitlab.com/bamhm182/docker-ssh-tunnels.git
cd docker-ssh-tunnels
./tunnels.py -s
```

#### Note on Digital Ocean Pricing

At the cheapest tier, Digital Ocean charges $0.007/hr while the Droplet exists,
regardless of wheter or not the Droplet is running. It has a minimum of charge
of $0.01 and a maximum of $5 per month for the lowest Standard option. $0.007/hr
takes just over 29 days to reach $5 and this project is super easy to set up,
so there is very little benefit to keeping the Droplet around. With that, I
recommend that you delete the Droplet as soon as you are done using this tool
for a little while.
